<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
class UserModel extends Eloquent {
	protected $table = 'data_user';
	protected $hidden = array('password');
}