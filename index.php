<?php
session_start();
require('vendor/autoload.php');

$app = new \Slim\Slim();

/**
 * Pages routing
 */
$app->get('/', function() use($app){
	$app->render('home.php'); // home page
});

$app->get('/', function() use($app){
	$app->render('login.php'); // login page
});

/**
 * API routing includes
 */
include('routes/alumni.php');
include('routes/login.php');

$app->run();